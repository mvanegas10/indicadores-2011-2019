const { Pool } = require('pg')
const config = require('config')

const dbConfig = config.get('development.dbConfig')

module.exports = {
    connect: function() {
        return new Pool(dbConfig)
    }
}

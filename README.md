# Correspondencia de Indicadores de la Encuesta de Movilidad de Bogotá del 2011 al 2019

Este proyecto cuenta con dos carpetas:

- notebooks: Se encuentra el notebook para hacer la traducción entre el SQL de las consultas del 2011 (Access) a las consultas del 2019 (PostgreSQL).
- js: Se encuentra la aplicación (node js) que permite probar todas las consultas e imprime el resultado en dos partes: Procesadas (el número de consultas procesadas exitosamente) y Error (las consultas que no pudieron ser procesadas y su respectivo error).

## Instalación
### Dependencias
- npm >= 6.7.0
- node >= 11.10.1
- python >= 3.7.0

### Notebooks

Para instalar los requerimientos del notebook, debe ingresar a la carpeta, crear un ambiente virtual, activarlo e instalar las dependencias.
```
cd notebook
conda create --name env
conda activate env
pip install -r requirements.txt

```

### App Web

Para instalar los requermientos, debe ingresar a la carpeta e instalar las dependencias identificadas en el package.json y correr la aplicación.
```
cd js
npm install
npm run start

```